# WebGoat 8 (Java) with Contrast 
This web application is a learning platform that attempts to teach about common web security flaws. It contains generic security flaws that apply to most web applications. It also contains lessons that specifically pertain to
the .NET framework. The exercises in this app are intended to teach about web security attacks and how developers can overcome them.

## WARNING!
THIS WEB APPLICATION CONTAINS NUMEROUS SECURITY VULNERABILITIES WHICH WILL RENDER YOUR COMPUTER VERY INSECURE WHILE RUNNING! IT IS HIGHLY RECOMMENDED TO COMPLETELY DISCONNECT YOUR COMPUTER FROM ALL NETWORKS WHILE RUNNING!

## Google Chrome Note
Google Chrome performs filtering for reflected XSS attacks. These attacks will not work unless chrome is run with the argument `--disable-xss-auditor`.

### Contrast Instrumentation in pom.xml
This repo includes the components necessary to instrument contrast Assess/Protect with this Java Spring Boot application except for the contrast_security.yaml file containing the connection strings.

Specifically modified:

1. pom.xml includes the Contrast Agent as a dependency, copies the Agent from the .m2 directory into the project (all targets, and includes instructions for Spring Boot to start the Contrast Agent at run-timecd ...
2. The contrast_security.yaml (not included in the repo) has several overrides and flags to enable/disable Assess and Protect, and a few other specific environment variables.
3. This application does not use Docker or docker-compose.

contrast_security.yaml example:

api:<br>
&nbsp;&nbsp;url: https://apptwo.contrastsecurity.com/Contrast<br>
&nbsp;&nbsp;api_key: [REDACTED<br>
&nbsp;&nbsp;service_key: [REDACTED]<br>
&nbsp;&nbsp;user_name: [REDACTED]<br>
agent:<br>
&nbsp;&nbsp;java:<br>
&nbsp;&nbsp;&nbsp;&nbsp;standalone_app_name: WebGoat-8.2.3<br>
application:<br>
&nbsp;&nbsp;metadata: bU=PS, contact=steve.smith@contrastsecurity.com<br>
&nbsp;&nbsp;session_metadata: buildNumber=56, committer=Steve Smith<br>
server:<br>
&nbsp;&nbsp;name: Application-Server-dev<br>
&nbsp;&nbsp;environment: dev<br>
assess:<br>
&nbsp;&nbsp;enable: true<br>
protect:<br>
&nbsp;&nbsp;enable: false<br>

Your contrast_security.yaml file needs to be in the target/ directory _after_ the application built (otherwise the target directory doesn't yet exist); the location is explicitly referenced in the pom.xml.

# Requirements

1. Java JDK 17

## How to build and run

### 1. Building the application from source
Building the Spring Boot project will also include the Contrast Agent with path to the YAML (you have to supply the YAML).

./mvnw clean install -Dmaven.test.skip=true # I skip the integration tests to make it faster and have fewer failures


### 2. Running the application 
Starting the Spring Boot application will also start the Contrast Agent.

To run `webgoat`, execute the following command:

./mvnw spring-boot:run

WebGoat should be accessible at http://ip_address:8080.
